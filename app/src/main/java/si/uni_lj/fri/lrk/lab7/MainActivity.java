package si.uni_lj.fri.lrk.lab7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String ACTION_CLASSIFIER_TRAINING = "si.uni_lj.fri.lrk.lab7.TRAIN_CLASSIFIER";

    AccBroadcastReceiver mBcastRecv;


    MachineLearningManager mManager;

    Handler handler;

    Boolean mSensing;
    Boolean mTraining;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBcastRecv = new AccBroadcastReceiver();

        this.handler = new Handler();

        final Button controlButton = findViewById(R.id.btn_control);

        mSensing = false;
        mTraining = false;

        controlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSensing) {
                    controlButton.setText(R.string.txt_start);
                    stopSensing();
                } else {
                    controlButton.setText(R.string.txt_stop);
                    startSensing();
                }
            }
        });

        Switch sw = findViewById(R.id.sw_training);

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mTraining = true;

                    findViewById(R.id.tv_select).setVisibility(View.VISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.INVISIBLE);
                } else {
                    mTraining = false;

                    findViewById(R.id.tv_select).setVisibility(View.INVISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.INVISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.VISIBLE);
                }
            }
        });
        try {
            mManager =
                    MachineLearningManager.getMLManager(getApplicationContext());
        } catch (MLException e) {
            e.printStackTrace();
        }
        initClassifier();


    }


    @Override
    protected void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(this).registerReceiver(mBcastRecv,
                new IntentFilter(AccBroadcastReceiver.ACTION_SENSING_RESULT));
    }


    @Override
    protected void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBcastRecv);
    }

    void startSensing()
    {
        Log.d(TAG,"startSensing()");

        mSensing = true;


        handler.post(new Runnable() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(),
                        AccSenseService.class);
                startService(intent);
                handler.postDelayed(this, 5000);
            }
        });


    }

    void stopSensing()
    {

        Log.d(TAG,"stopSensing()");

        mSensing = false;

        this.handler.removeMessages(0);
        TextView t = findViewById(R.id.tv_result);
        t.setText("Inferred :");

        View v  = findViewById(R.id.container);
        v.setBackgroundColor(Color.WHITE);
    }


    void initClassifier() {
        Log.d(TAG,"initClassifier");

        Feature accMean = new FeatureNumeric("accMean");
        Feature accVar = new FeatureNumeric("accVar");
        Feature accMCR = new FeatureNumeric("accMCR");

        ArrayList<String> classValues = new ArrayList<String>();
        classValues.add(getResources().getString(R.string.txt_gesture_1));
        classValues.add(getResources().getString(R.string.txt_gesture_2));
        classValues.add(getResources().getString(R.string.txt_gesture_3));

        Feature movement = new FeatureNominal("movement", classValues);
        ArrayList<Feature> features = new ArrayList<Feature>();
        features.add(accMean);
        features.add(accVar);
        features.add(accMCR);

        features.add(movement);
        Signature signature = new Signature(features, features.size()-1);
// TODO: a try-catch block is needed here
        try {
            mManager.addClassifier(Constants.TYPE_NAIVE_BAYES, signature,
                    new ClassifierConfig(),"movementClassifier");
        } catch (MLException e) {
            e.printStackTrace();
        }




    }


    public void recordAccData(float mean, float variance, float MCR) {

        Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + MCR);

        Switch s = findViewById(R.id.sw_training);

        if (s.isChecked()) {
            // TODO: get the label of the selected radio button
            RadioGroup rg = findViewById(R.id.radioGroup);
            int selectedId = rg.getCheckedRadioButtonId();
            RadioButton rb = (RadioButton) findViewById(selectedId);
            String label = rb.getText().toString();

            // TODO: send data to TrainClassifierService
            Intent mIntent = new Intent(MainActivity.this,
                    TrainClassifierService.class);
            mIntent.putExtra("accMean", mean);
            mIntent.putExtra("accVar", variance);
            mIntent.putExtra("accMCR", MCR);
            mIntent.putExtra("label", label);
            mIntent.setAction(ACTION_CLASSIFIER_TRAINING);
            startService(mIntent);



        } else {

            // TODO: Do the inference (classification) and set the result (also screen background colour)
            Classifier c = mManager.getClassifier("movementClassifier");
            ArrayList<Value> instanceValues = new ArrayList<Value>();
            Value meanValue = new Value((double) mean, Value.NUMERIC_VALUE);
            instanceValues.add(meanValue);
            Value varValue = new Value((double)variance, Value.NUMERIC_VALUE);
            Value MCRValue = new Value((double)MCR, Value.NUMERIC_VALUE);
            instanceValues.add(varValue);
            instanceValues.add(MCRValue);

            Instance instance = new Instance(instanceValues);
            ArrayList<Instance> instances = new ArrayList<Instance>();
            instances.add(instance);
// TODO: add two more features here
// TODO: a try-catch block is needed here
            try {

                Value inference = c.classify(instance);
                String valuestring = inference.getValue().toString();
                System.out.println("here is else : "+valuestring);
                View v = findViewById(R.id.container);
                TextView t = findViewById(R.id.tv_result);

                if(valuestring.equals(getString(R.string.txt_gesture_1)))
                {
                    t.setText("Inferred : "+getString(R.string.txt_gesture_1));
                    v.setBackgroundColor(Color.BLUE);

                }else if(valuestring.equals(getString(R.string.txt_gesture_2)))
                {
                    t.setText("Inferred : "+getString(R.string.txt_gesture_2));
                    v.setBackgroundColor(Color.RED);

                }else if(valuestring.equals(getString(R.string.txt_gesture_3)))
                {
                    t.setText("Inferred : "+getString(R.string.txt_gesture_3));
                    v.setBackgroundColor(Color.YELLOW);
                }
            }catch(MLException e)
            {
                e.printStackTrace();
            }



        }
    }


    public class AccBroadcastReceiver extends BroadcastReceiver {

        public static final String ACTION_SENSING_RESULT = "si.uni_lj.fri.lrk.lab7.SENSING_RESULT";
        public static final String MEAN = "mean";
        public static final String VARIANCE = "variance";
        public static final String MCR = "MCR";

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, " AccBroadcastReceiver onReceive...");

            float mean = intent.getFloatExtra(MEAN, 0);
            float variance = intent.getFloatExtra(VARIANCE, 0);
            float mcr = intent.getFloatExtra(MCR, 0);

            Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + mcr);
            recordAccData(mean, variance, mcr);
        }
    }

}
